import os
import asyncio

from bs4 import BeautifulSoup
import requests
from datetime import datetime
from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import *

app = Flask(__name__)

# Channel Access Token
line_bot_api = LineBotApi(
    '56y+xkdnEQcJUHYiTDHOIblyi1fkq6A4vAgHoolmUKBvov5dR3DZak11S3AfP5rMIrIINI5KG49XWneJxxLUwHdh+FXCDhX9keQKcgAlSRcJx4cEAKJKdphwKRzSktajFSjgbwHC32cVUg0WlwAzlgdB04t89/1O/w1cDnyilFU=')
# Channel Secret
handler = WebhookHandler('b0ab70a16449b5784cbc9f7c9f1b1a39')


# 監聽所有來自 /callback 的 Post Request
@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']
    # get request body as text
    body = request.get_data(as_text=True)
    app.logger.info("Request body: " + body)
    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'


# async def stock(A):
#     line_bot_api.push_message(TextSendMessage(text=f'你要查那一檔股票？請輸入代號？'))
#     x = input("請輸入股票代號")
#     stock = x  # 尋找股票號碼2347 聯強的資訊
#     url = "https://tw.stock.yahoo.com/q/q?s=" + stock
#     list_req = requests.get(url)
#     soup = BeautifulSoup(list_req.content, "lxml")
#     # getstock=soup.find('b').text
#     # print(getstock)
#     getstock = soup.findAll("b")[1].text
#     await
#     return getstock


@handler.add(MessageEvent, message=TextMessage)
def handle_message(event):
    input_text = event.message.text
    if input_text == "查匯率":
        resp = requests.get('https://tw.rter.info/capi.php')
        currency_data = resp.json()
        usd_to_twd = currency_data['USDTWD']['Exrate']
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=f'美元 USD 對台幣 TWD：1:{usd_to_twd}'))
    elif input_text == '現在時間':
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        line_bot_api.reply_message(
            event.reply_token, TextSendMessage(text=f"台北時間：{current_time}"))
    elif input_text == "現在幾度" or "溫度":
        Rdata = requests.get(
            'https://opendata.cwb.gov.tw/api/v1/rest/datastore/O-A0003-001?Authorization=CWB-EE754089-8A36-4D05-ADA2-56FCF1FF5E37&locationName=%E8%87%BA%E5%8C%97,&elementName=TEMP&parameterName=CITY').json()
        Temp = Rdata["records"]["location"][0]["weatherElement"]
        Mdict = dict(enumerate(Temp))
        o1 = Mdict[0]['elementValue']
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=f'現在「台北」的目前溫度是：{o1}'))
    elif input_text == "股票":

        stock(x)
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text=f'現在「股票號碼{stock} 」的目前股價是：{getstock}'))
# else:
#     message = TextSendMessage(text=event.message.text)
#     line_botdfs_api.reply_message(event.reply_token, message)


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
