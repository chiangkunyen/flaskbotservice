import os
import asyncio
import pandas as pd

from bs4 import BeautifulSoup
import requests
from datetime import datetime
from flask import Flask, request, abort

from linebot import (
    LineBotApi, WebhookHandler
)
from linebot.exceptions import (
    InvalidSignatureError
)
from linebot.models import *

notin = []

app = Flask(__name__)

# Channel Access Token
line_bot_api = LineBotApi(
    '56y+xkdnEQcJUHYiTDHOIblyi1fkq6A4vAgHoolmUKBvov5dR3DZak11S3AfP5rMIrIINI5KG49XWneJxxLUwHdh+FXCDhX9keQKcgAlSRcJx4cEAKJKdphwKRzSktajFSjgbwHC32cVUg0WlwAzlgdB04t89/1O/w1cDnyilFU=')
# Channel Secret
handler = WebhookHandler('b0ab70a16449b5784cbc9f7c9f1b1a39')


# 監聽所有來自 /callback 的 Post Request
@app.route("/callback", methods=['POST'])
def callback():
    # get X-Line-Signature header value
    signature = request.headers['X-Line-Signature']
    # get request body as text
    body = request.get_data(as_text=True)
    print(body)
    app.logger.info("Request body: " + body)
    # handle webhook body
    try:
        handler.handle(body, signature)
    except InvalidSignatureError:
        abort(400)
    return 'OK'


async def stock(A):
    # await line_bot_api.reply_message(TextSendMessage(text=f'你要查那一檔股票？請輸入代號？'))
    stoc = line_bot_api.reply_message(TextSendMessage(text=f'你要查那一檔股票？請輸入代號？'))  # 尋找股票號碼2347 聯強的資訊
    url = "https://tw.stock.yahoo.com/q/q?s=" + stoc
    list_req = requests.get(url)
    soup = BeautifulSoup(list_req.content, "lxml")
    getstock = soup.findAll("b")[1].text
    Remsg = "現在查詢的股票現價是" + getstock
    await line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=Remsg))


async def ER(B):
    resp = requests.get('https://tw.rter.info/capi.php')
    currency_data = resp.json()
    ERout = currency_data['USDTWD']['Exrate']
    Remsg = "美元兌換台幣：" + ERout
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=Remsg))


async def rpweather(C):
    Rdata = requests.get(
        'https://opendata.cwb.gov.tw/api/v1/rest/datastore/O-A0003-001?Authorization=CWB-EE754089-8A36-4D05-ADA2-56FCF1FF5E37&locationName=%E8%87%BA%E5%8C%97,&elementName=TEMP&parameterName=CITY').json()
    Temp = Rdata["records"]["location"][0]["weatherElement"]
    Mdict = dict(enumerate(Temp))
    Weatherout = Mdict[0]['elementValue']
    Remsg = "現在「台北」溫度是：" + Weatherout
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=Remsg))


async def rpTime(D):
    now = datetime.now()
    TimeNOW = now.strftime("%H:%M:%S")
    Remsg = "現在時間是：" + TimeNOW
    line_bot_api.reply_message(
        event.reply_token,
        TextSendMessage(text=Remsg))


@handler.add(MessageEvent, message=TextMessage)
# async def handle_message(event, getstock, ERout, Weatherout, TimeNOW):a
async def handle_message(event):
    msg = event.message.text
    Remsg = "BOT START！！"
    stockIn = ["查股票", "今天股巿行情", "股票"]
    ERIn = ["查匯率", "查匯率", "匯率", "目前匯率", "今天的匯率", "美金兌台幣匯率", "美金兌台幣"]
    weatherIn = ["氣象", "溫度", "天氣", "現在溫度"]
    TNowIn = ["查時間", "現在時間", "時間", "幾點了？"]
    line_botdfs_api.reply_message(event.reply_token, Remsg)

    if msg == "股票":
        asyncio.create_task(stock(1))
        stock()
    elif msg in ERIn:
        asyncio.create_task(ER(2))
        await ER()
    elif msg in weatherIn:
        asyncio.create_task(rpweather(3))
        await rpweather()
    elif msg in TNowIn:
        asyncio.create_task(rpTime(4))
        await rpTime()

    else:
        line_bot_api.reply_message(
            event.reply_token,
            TextSendMessage(text="請你說中文好嗎？阿鬼"))
        cvot = notin.append(msg)
        df = pd.DataFrame(cvot)
        df.to_csv("savedict.csv", encoding='utf8')


#
# rpweather()
# Remsg = Weatherout
# elif msg in ERIn:
# elif keywords(msg, TNowIn):
# #     message = TextSendMessage(text=event.message.text)
# #     line_botdfs_api.reply_message(event.reply_token, message)
#
# line_bot_api.reply_message(
#     event.reply_token,
#     TextSendMessage(text=Rems
# Remsg = ""

if __name__ == "__main__":
    asyncio.run(handle_message())
    app.run()
    port = int(os.environ.get('PORT', 5000))
