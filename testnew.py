def check_stock_value():  # 即時股票價格
    import datetime
    import time
    import requests
    from bs4 import BeautifulSoup
    stock_number = str(input("輸入股票代號:"))
    url = "https://tw.stock.yahoo.com/q/q?s=" + stock_number
    list_req = requests.get(url)
    soup = BeautifulSoup(list_req.content, "html.parser")
    try:
        stock_name = soup.find(href="/q/bc?s=" + stock_number).text
        print("股票名稱:", stock_name)

        time = soup.find_all("td", align="center", bgcolor="#FFFfff")[0].text
        print("時間:", time)

        getstock = eval(soup.findAll("b")[1].text)
        print("即時股價:", getstock)

        value = soup.findAll("font")[2].text
        print("漲跌:", value[0:6])

        b = eval(soup.find_all("td", align="center", bgcolor="#FFFfff")[6].text)

        Quote = ((getstock - b) / b) * 100
        if Quote >= 0:
            print("漲跌幅:" + "+" + "%5.2f" % Quote + "%")
        else:
            print("漲跌幅:%6.2f" % Quote + "%")
        return (stock_name, stock_number)
    except:
        print("股票代碼錯誤")


check_stock_value()